#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <stdlib.h>
#include "type.h"
#include "stack.h"

using namespace std;

vector<string> split(const char *str, char c = ' ')
{
    vector<string> result;

    do
    {
        const char *begin = str;

        while(*str != c && *str)
            str++;

        result.push_back(string(begin, str));
    } while (0 != *str++);

    return result;
}

int main(int argc, char ** argv)
{
    if(argc != 3)
    {
        cout << "Error: this program requires two files as arguments." << endl;
        return 1;
    }
    ifstream inFile(argv[1]);
    if(inFile.is_open())
    {
        ofstream outFile(argv[2]);
        if(outFile.is_open())
        {
            vector<string> program;
            Stack * current = new Stack();
            string line;
            while(getline(inFile,line))
            {
                program.push_back(line);
            }
            current->lineNum = 0;
            bool inFunction = false;
            while(current->lineNum < (int)program.size())
            {
               vector<string> tokenized = split(program.at(current->lineNum).c_str(), ' ');
               if(program.at(current->lineNum) == "")
               {
               }
               else if(tokenized.at(0) == "FUNCTION")
               {
                   Function * curFunc = new Function;
                   curFunc->line = current->lineNum;
                   for(int i=2; i<(int)tokenized.size(); i++)
                   {
                        curFunc->argNames.push_back(tokenized.at(i));
                   }
                   current->functions[tokenized.at(1)] = (*curFunc);
                   inFunction = true;
               }
               else if(tokenized.at(0) == "RETURN" && !inFunction)
               {
                   current->previous->variables[current->returnName] = current->variables[tokenized.at(1)];
                   Stack * newCur = current->previous;
                   delete current;
                   current = newCur;
               }
               else if(tokenized.at(0) == "END")
               {
                   inFunction = false;
               }
               else if(tokenized.at(0) == "STORE" && !inFunction)
               {
                   current->variables[tokenized.at(1)] = new Type(atoi(tokenized.at(2).c_str()));
               }
               else if(tokenized.at(0) == "ADD" && !inFunction)
               {
                   current->variables[tokenized.at(3)] = new
                   Type(static_cast<Type*>(current->variables[tokenized.at(1)])->intValue() +
                        static_cast<Type*>(current->variables[tokenized.at(2)])->intValue());
               }
               else if(tokenized.at(0) == "MULT" && !inFunction)
               {
                   current->variables[tokenized.at(3)] = new
                   Type(static_cast<Type*>(current->variables[tokenized.at(1)])->intValue() *
                        static_cast<Type*>(current->variables[tokenized.at(2)])->intValue());
               }
               else if(tokenized.at(0) == "PRINT" && !inFunction)
               {
                   outFile << static_cast<Type*>(current->variables[tokenized.at(1)])->intValue() << endl;
               }
               else if(tokenized.at(0) == "IFNEQ" && !inFunction)
               {
                   if( static_cast<Type*>(current->variables[tokenized.at(1)])->intValue() !=
                       static_cast<Type*>(current->variables[tokenized.at(2)])->intValue())
                   {
                       current->lineNum = atoi(tokenized.at(3).c_str()) - 2;
                   }
               }
               else if(tokenized.at(0) == "IFEQ" && !inFunction)
               {
                   if( static_cast<Type*>(current->variables[tokenized.at(1)])->intValue() ==
                       static_cast<Type*>(current->variables[tokenized.at(2)])->intValue())
                   {
                       current->lineNum = atoi(tokenized.at(3).c_str()) - 2;
                   }
               }
               else if(!inFunction)
               {
                   // Custom function call
                   Function f = current->functions[tokenized.at(0)];
                   Stack * s = new Stack();
                   s->previous = current;
                   s->variables.insert(current->variables.begin(), current->variables.end());
                   s->functions.insert(current->functions.begin(), current->functions.end());
                   for(int i=0; i<(int)f.argNames.size(); i++)
                   {
                        s->variables[f.argNames.at(i)] = current->variables[tokenized.at(i+1)];
                   }
                   s->returnName = tokenized.at((int)tokenized.size()-1);
                   current = s;
                   current->lineNum = f.line;
               }
               current->lineNum++;
            }
            outFile.close();
            return 0;
        }
        else
        {
            cout << "Error: could not write to file " << argv[2] << "." << endl;
            inFile.close();
            return 1;
        }
    }
    else
    {
        cout << "Error: file " << argv[1] << " not found." << endl;
        return 1;
    }
}
