#ifndef STACK_H
#define STACK_H

#include <map>
#include <string>
#include <vector>
#include "type.h"

class Function
{
public:
    int line;
    std::vector<std::string> argNames;
    Function();
};

class Stack
{
public:
    int lineNum;
    std::string returnName;
    Stack * previous;
    std::map<std::string,void*> variables;
    std::map<std::string,Function> functions;
    Stack();
};
#endif
