#ifndef TYPE_H
#define TYPE_H

#include <string>

template <class T> class Value
{
protected:
    T value;
public:
    Value(T val) : value(val) {}

    T getValue()
    {
        return value;
    }
};

class Type
{
protected:
    void* val;
    std::string t;
public:
    Type(int  v) : val(new Value<int >(v)), t("int" ) {}
    Type(char c) : val(new Value<char>(c)), t("char") {}
    Type(bool b) : val(new Value<bool>(b)), t("bool") {}
    Type(std::string s) : val(new Value<std::string>(s)), t("string")
    int  intValue() { return (static_cast<Value<int>*>(val))->getValue(); }
    char charValue() { return (static_cast<Value<char>*>(val))->getValue(); }
    bool boolValue() { return (static_cast<Value<bool>*>(val))->getValue(); }
    std::string stringValue() { return (static_cast<Value<std::string>*>(val))->getValue(); }
    std::string returnType() { return t; }
};

#endif
